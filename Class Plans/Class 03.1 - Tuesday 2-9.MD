## Agenda

### Planning Meeting: Team Scrum Plum (11:00) & Team Auth (11:30) Planning Meeting
* Team Auth send a "spy" to Team Scrum Plum meeting and vice versa.
* We will go through all items in the "to do" list.
* May be some items I have moved to "revise" which means that the issue has something very minor that must be fixed by the end of the day to be moved into "to so" 
* You will present each issue in the "to do" column and talk about what is involved and the weight
    * Make sure that all issues have a lead assigned and a weight
    * Make sure that you have sufficient work for the sprint - **plan for more!!**
    * Make sure that tasks can be undertaken in parllel

### Standup Meeting: Team 3- 12:00 Noon
* Send "spy" to Team Auth and Team Scrum Plum meetings
* Come prepared for each team member to answer these three questions:
    * What have you done since last Thursday?
    * What will you do before class on Thursday?
    * What if anything is blocking your progress? 
