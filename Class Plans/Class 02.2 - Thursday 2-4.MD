## Agenda

### 11:00 Team Scrum Plum & 11:20 Team Auth - Stand up meeting
* Reminder that you need to have your Developer's Board ready for review by 5:00 PM Monday 2/8
* Do you have all permissions that you need? Let me know if you do not. 
* Come prepared for each team member to answer these three questions:
    * What have you done since last Thursday?
    * What will you do before class on Thursday?
    * What if anything is blocking your progress? 

### Team 3 - 11:40
* Pre-planning meeting
* Does your team have a name? 
* Reminder that you need your Developer's Board ready for review by 5:00 PM Wednesday 2/10
* Who are your CoP people?
    * FrontEnd/UI
    * Backend
    * REST API
* Have you set up your board?
* Have you started thinking about Working Agreement?
* Have you read existing Retrospectives?
* Do you understand how to use the templates?
* Do you know what your first tasks are?
* Goal of first sprint:  Position team so that you are ready to develop
    * Should include learning goals
    * Don't forget to include Docker and CI/CD
