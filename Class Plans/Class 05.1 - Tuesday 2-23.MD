## Agenda
### 11:00 Team Auth Review Meeting 
### 11:30 Team Scrum Plum Review Meeting
### 12:00 Team View Standup Meeting

### Review Meeting Agenda:
* Deadline: Have things marked as "done" by 5:00 PM Monday 2/22
    * I'll go through and approve or reject
* One person presents the current status of the project
    * 10 minutes 
    * Overview of your segment of the project
    * Identification of the major tasks involved in the sprint 
        * Degree of completeness of each task 
        * What is left to be done 
    * What roadblocks were encountered?
    * How will these roadblocks be addressed in the future?
    * What are the next steps in the work? 
    * What was the big "take away" from this sprint? 
* Collectively decide on next steps.  
