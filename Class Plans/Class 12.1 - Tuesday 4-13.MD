## Agenda
### Team Auth Standup Meeting - 11:00
* Send "spy" to Team View Planning Meeting
### Team Scrum Plums Standup Meeting - 11:15
* Send "spy" to Team View Planning Meeting
### Team View Planning Meeting - 11:30
* We will go through all items in the "to do" list.
* May be some items I have moved to "revise" which means that the issue has something very minor that must be fixed by the end of the day to be moved into "to so"
* You will present each issue in the "to do" column and talk about what is involved and the weight
* Make sure that all issues have a lead assigned and a weight
* Make sure that you have sufficient work for the sprint - plan for more!!
* Make sure that tasks can be undertaken in parllel






