Week | Date | Team Auth, Team Scrum Plums | Team View
:-- | :-- | :-- | :--
1 | 1/26 | Introduction and Team Formation | Introduction and Team Formation 
1 | 1/28 | Pre-Planning | Observe - send someone to observe Team Auth & Team Scrum Plum planning
2 | 2/2 | SNOW DAY - No Class | SNOW DAY - No Class
2 | 2/4 | Continue planning in class | Pre-Planning
3 | 2/9 | Sprint 1 Planning Meeting | Continue planning in class <br /> Send observer to Team Auth & Team Scrum Plum Planning Meeting 
3 | 2/11 | Standup Meeting <br /> Continue working in class <br /> Send observer to Team View Planning | Sprint 1 Planning Meeting 
4 | 2/16 | Standup Meeting <br /> Continue working in class |  Standup Meeting <br /> Continue working in class 
4 | 2/18 | Standup Meeting <br /> Continue working in class |  Standup Meeting <br /> Continue working in class
5 | 2/23 | Sprint 1 Review | Standup Meeting <br /> Continue working in class <br /> Send observer to Team Auth & Team Scrum Plum Reviews 
5 | 2/25 | Sprint 1 Retrospective | Standup Meeting <br /> Continue working in class 
6 | 3/2 | Sprint 2 Planning | Standup Meeting <br /> Continue working in class <br /> Send observer to Team Auth & Team Scrum Plum Planning  
6 | 3/4 | Standup Meeting <br /> Continue working in class <br /> Send observer to Team View Review  | Sprint 1 Review
7 | 3/9 | Standup Meeting <br /> Continue working in class | Sprint 1 Retrospective 
7 | 3/11 | Standup Meeting <br /> Continue working in class <br /> Send observer to Team View Planning | Sprint 2 Planning Meeting
8 | 3/16 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class 
8 | 3/18 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class
9 | 3/23 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class 
9 | 3/25 | Sprint 2 Review | Standup Meeting <br /> Continue working in class <br /> Send observer to Team Auth & Team Scrum Plum Reviews 
10 | 3/30 | Sprint 2 Retrospective | Standup Meeting <br /> Continue working in class 
10 | 4/1 | No Class! | No Class!
11 | 4/6 | Sprint 3 Planning <br /> Send observer to Team View Review | Sprint 1 Review <br /> Send observer to Team Auth & Team Scrum Plum Planning 
11 | 4/8 | Standup Meeting <br /> Continue working in class   | Sprint 2 Retrospective
12 | 4/13 | Standup Meeting <br /> Continue working in class <br /> Send observer to Team View Planning | Sprint 3 Planning
12 | 4/15 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class
13 | 4/20 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class 
13 | 4/22 | Standup Meeting <br /> Continue working in class | Standup Meeting <br /> Continue working in class
14 | 4/27 | Sprint 3 Review | Standup Meeting <br /> Continue working in class <br /> Send observer to Team Auth & Team Scrum Plum Review
14 | 4/29 | Sprint 3 Retrospective | Sprint 3 Review and Retrospective 
15 | 5/3 | Midnight - [Final Essay](https://gitlab.com/heidijcellis/cs-492/-/blob/master/Final%20Essay.MD) | Midnight - [Essay](https://gitlab.com/heidijcellis/cs-492/-/blob/master/Final%20Essay.MD)
15 | 5/6 | 10:15 AM Presentation | 10:15 AM Presentation | 

The table below shows the number of work sessions for each sprint for each team. Sprint 1 does not count the work session between pre-planning and planning. This table is provided to help aid planning.

Sprint | Work Sessions<br> Team Auth & Team Scrum Plum | Work Sessions <br> Team View
:--: | :--: | :--:
1 | 5 | 7
2 | 5 | 4
3 | 5 | 4
Total | 15 | 15

## Copyright and License
#### &copy; 2021 Heidi Ellis, Stoney Jackson and Karl Wurst
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA2019

